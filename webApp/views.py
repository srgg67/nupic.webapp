from django.http import HttpResponse
# responsible for templates getting
from django.template.loader import get_template
# responsible fot using variables within template
from django.template import Context
from django.shortcuts import render_to_response

def home(request):
	return render_to_response( "default.html",
        {
            'content':
                "Home page"
        })