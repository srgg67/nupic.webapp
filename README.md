###The Nupic web application on Python/Django. 
It is a simpliest example of using [Nupic](https://github.com/numenta/nupic); it has been created for educational purpose. 

To use it is enough the following skills: 

* Python - basic level

* Minimal experience of web building
***
The tutorial in .pdf is here: [http://srgg6701/srggnupicwebapp/blob/gh-pages/nupic_webapp_development.pdf](http://srgg6701.github.io/srggnupicwebapp/nupic_webapp_development.pdf)
