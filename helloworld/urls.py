from django.conf.urls import patterns, url
#from django.conf.urls.defaults import *
import views
urlpatterns = patterns ('',
        #helloworld
        url(r'^$', 'helloworld.views.helloworld'),
        #helloworld/test
        url(r'^test/', 'helloworld.views.test'),
    )