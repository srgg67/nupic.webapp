from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render_to_response

# get data from the test.py module
def test(request):
	import test
	html=""
	for arr in test.arr1:
		html+='<div>'
		cnt = 0
		for d in arr:
			if cnt:
				html+=', '
			html = html+str(d)
			cnt +=1
		html+='</div>'
	return render_to_response("default.html", { 'html': html })

def makeHTML(header,array):
	html= "<h3>"+header+"</h3>"
	html+="<div>"
	for lst in array:
		for arr in lst:
			cnt = 0
			for d in arr:
				if cnt:
					html+=", "
				cnt+=1
				html+=str(d)
		html+="<hr/>"
	html+="</div>"
	return html

# get data from the helloworld.py module
def helloworld(request):
	import helloworld
	html=makeHTML("Random vectors", helloworld.arr_data_random)
	html+=makeHTML("Identical vectors", helloworld.arr_data_identical)
	return render_to_response( "default.html", { 'html': html })